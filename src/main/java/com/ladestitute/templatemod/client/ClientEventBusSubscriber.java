package com.ladestitute.templatemod.client;

import com.ladestitute.templatemod.TemplateMain;
import com.ladestitute.templatemod.registry.BlockInit;
import com.ladestitute.templatemod.util.TemplateKeyboardUtil;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = TemplateMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
        ItemBlockRenderTypes.setRenderLayer(BlockInit.TEMPLATE_BLOCK.get(), RenderType.cutout());
        TemplateKeyboardUtil.register();
    }
}

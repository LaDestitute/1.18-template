package com.ladestitute.templatemod.registry;

import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;

public class PlacedFeatureInit {
    public static final PlacedFeature TEMPLATE_PLACEMENT =
            PlacementUtils.register("template_placement", FeatureInit.TEMPLATE_CONFIG.placed());
}

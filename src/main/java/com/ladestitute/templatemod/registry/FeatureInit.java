package com.ladestitute.templatemod.registry;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.placement.BlockPredicateFilter;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;

public class FeatureInit {

    public static final ConfiguredFeature<RandomPatchConfiguration, ?> TEMPLATE_CONFIG = FeatureUtils.register("template_config",
            Feature.FLOWER.configured(new RandomPatchConfiguration(5, 20, 20, () -> {
                return Feature.SIMPLE_BLOCK.configured(new SimpleBlockConfiguration(BlockStateProvider.simple(
                                BlockInit.TEMPLATE_BLOCK.get().defaultBlockState())))
                        .placed(InSquarePlacement.spread(),  PlacementUtils.HEIGHTMAP_WORLD_SURFACE,
                                BlockPredicateFilter.forPredicate(BlockPredicate.wouldSurvive(Blocks.DANDELION.defaultBlockState(), BlockPos.ZERO)));
                //From testing in 1.18, tries is a short range...1 is rare as it may be while 5 is quite a bit, anything +10 seems plentiful,
                // so tweak until it looks right
                //The wouldSurvive predicate is useful for plants but also ensuring a block doesn't spawn on water
            })));

    private static <FC extends FeatureConfiguration>ConfiguredFeature<FC, ?> register(String name,
                                                                                      ConfiguredFeature<FC, ?> configuredFeature) {
        return Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, name, configuredFeature);
    }
}

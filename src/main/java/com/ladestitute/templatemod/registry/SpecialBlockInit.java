package com.ladestitute.templatemod.registry;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SpecialBlockInit {
    //A class for blocks we don't want automatically registered into our creative tab and/or put into a different one
    public static final DeferredRegister<Block> SPECIAL_BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            TemplateMain.MOD_ID);
}

package com.ladestitute.templatemod.registry;

import com.ladestitute.templatemod.TemplateMain;
import com.ladestitute.templatemod.blocks.TemplateBlock;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            TemplateMain.MOD_ID);

    public static final RegistryObject<Block> TEMPLATE_BLOCK = BLOCKS.register("template_block",
            () -> new TemplateBlock(PropertiesInit.TEMPLATE_BLOCK));
}

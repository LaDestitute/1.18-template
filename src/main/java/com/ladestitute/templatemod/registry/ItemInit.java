package com.ladestitute.templatemod.registry;

import com.ladestitute.templatemod.TemplateMain;
import com.ladestitute.templatemod.items.TemplateItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            TemplateMain.MOD_ID);

    public static final RegistryObject<Item> TEMPLATE_ITEM = ITEMS.register("template_item",
            () -> new TemplateItem(new Item.Properties().tab(TemplateMain.BLOCKS_TAB)));
}

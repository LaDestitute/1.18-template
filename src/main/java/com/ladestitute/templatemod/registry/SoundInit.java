package com.ladestitute.templatemod.registry;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, TemplateMain.MOD_ID);

    public static final RegistryObject<SoundEvent> EXAMPLE_SOUND = SOUNDS.register("block.example_sound",
            () -> new SoundEvent(new ResourceLocation(TemplateMain.MOD_ID, "block.example_sound")));
}

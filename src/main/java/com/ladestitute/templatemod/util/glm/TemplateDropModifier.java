package com.ladestitute.templatemod.util.glm;

import com.google.gson.JsonObject;
import com.ladestitute.templatemod.TemplateMain;
import com.ladestitute.templatemod.registry.BlockInit;
import com.ladestitute.templatemod.util.TemplateConfig;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Random;

public class TemplateDropModifier {
    public static final DeferredRegister<GlobalLootModifierSerializer<?>> GLM = DeferredRegister.create(ForgeRegistries.LOOT_MODIFIER_SERIALIZERS, TemplateMain.MOD_ID);
    public static final RegistryObject<BlockDropModifier.Serializer> TEMPLATEMOD_DROPS = GLM.register("templatemod_drops", BlockDropModifier.Serializer::new);

    public static class BlockDropModifier extends LootModifier {

        public BlockDropModifier(LootItemCondition[] lootConditions) {
            super(lootConditions);
        }

        @Nonnull
        @Override
        protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
            if (context.hasParam(LootContextParams.BLOCK_STATE)) {
                BlockState state = context.getParamOrNull(LootContextParams.BLOCK_STATE);
                Block block = state.getBlock();
                //Example drop modifier
                if (block == Blocks.STONE ||
                        block == Blocks.BLACKSTONE) {
                    if (TemplateConfig.getInstance().exampleboolean()) {
                        generatedLoot.clear();
                        generatedLoot.add(new ItemStack(BlockInit.TEMPLATE_BLOCK.get(), 1));
                    }

                }
                //
            }
            return generatedLoot;
        }

        private static class Serializer extends GlobalLootModifierSerializer<BlockDropModifier> {

            @Override
            public BlockDropModifier read(ResourceLocation location, JsonObject jsonObject, LootItemCondition[] lootConditions) {
                return new BlockDropModifier(lootConditions);
            }

            @Override
            public JsonObject write(BlockDropModifier instance) {
                return makeConditions(instance.conditions);
            }
        }
    }
}


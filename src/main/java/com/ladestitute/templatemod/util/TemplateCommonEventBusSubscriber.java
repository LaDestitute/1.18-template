package com.ladestitute.templatemod.util;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = TemplateMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TemplateCommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {

    }

    //The proper way to register caps in 1.18
   // @SubscribeEvent
   // public void registerCaps(RegisterCapabilitiesEvent event) {
     //   event.register(IOurCapability.class);
   // }

}

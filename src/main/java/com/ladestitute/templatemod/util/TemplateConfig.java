package com.ladestitute.templatemod.util;

import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

public class TemplateConfig {
    //Example config class
    private static final TemplateConfig INSTANCE;

    public static final ForgeConfigSpec SPEC;

    static {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
        Pair<TemplateConfig, ForgeConfigSpec> specPair =
                new ForgeConfigSpec.Builder().configure(TemplateConfig::new);
        INSTANCE = specPair.getLeft();
        SPEC = specPair.getRight();
    }

    private final ForgeConfigSpec.BooleanValue exampleboolean;
    private final ForgeConfigSpec.IntValue exampleinteger;

    private TemplateConfig(ForgeConfigSpec.Builder configSpecBuilder) {
        exampleboolean = configSpecBuilder
                .comment("I'm a boolean.")
                .define("exampleboolean", false);
        exampleinteger = configSpecBuilder
                .comment("I'm an integer.")
                .defineInRange("exampleinteger", 5, 1, 10);
    }

    public static TemplateConfig getInstance() {
        return INSTANCE;
    }
    // Query operations
    public boolean exampleboolean() { return exampleboolean.get(); }
    public int exampleinteger() { return exampleinteger.get(); }

    //Change operations
    public void changeexampleboolean(boolean newValue) {
        exampleboolean.set(newValue);
    }
    public void changeexampleinteger(int newValue) {
        exampleinteger.set(newValue);
    }

    public void save() {
        SPEC.save();
    }
}

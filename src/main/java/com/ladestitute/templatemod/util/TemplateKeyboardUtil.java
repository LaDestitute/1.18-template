package com.ladestitute.templatemod.util;

import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.ClientRegistry;
import org.lwjgl.glfw.GLFW;

public class TemplateKeyboardUtil {

    public static KeyMapping templatekey;

    public static void register()
    {
        templatekey = new KeyMapping("templatekey", GLFW.GLFW_KEY_ENTER, "key.categories.templatemod");

        ClientRegistry.registerKeyBinding(templatekey);
    }

}


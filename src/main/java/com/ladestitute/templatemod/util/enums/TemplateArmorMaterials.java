package com.ladestitute.templatemod.util.enums;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Blocks;

public enum TemplateArmorMaterials implements ArmorMaterial
{
    TEMPLATE_ARMOR("template_armor", 200, new int[] {20, 20, 20, 20}, 0, Blocks.AIR.asItem(),
            SoundEvents.ARMOR_EQUIP_LEATHER, 0.0f, 0.0f);

    private String name;
    private final SoundEvent soundEvent;
    private int durability, enchantability;
    private int[] dmgReductionAmounts;
    private float toughness, knockbackResistance;
    private Item repairItem;
    private static final int[] max_damage_array = new int[] {13, 15, 16, 11};

    private TemplateArmorMaterials(String name, int durability, int[]dmgReductionAmounts, int enchantability, Item repairItem, SoundEvent soundEvent, float toughness, float knockbackResistance)
    {
        this.name = name;
        this.soundEvent = soundEvent;
        this.durability = durability; //helmet*11, chest*16, leg*15, boots*13   //Dia 33, Iron 15, Gold 7, Leather 5
        this.enchantability = enchantability; //Leather 15, Chain 12, Iron 9, Gold 25, Dia 10, 1.16Netherite 15
        this.dmgReductionAmounts = dmgReductionAmounts; //{Boots, Leg, Chest, Helmet}
        this.toughness = toughness; //Dia 2, 1.16Netherite 3
        this.repairItem = repairItem;
        this.knockbackResistance = knockbackResistance;

    }

    @Override
    public int getDurabilityForSlot(EquipmentSlot slot)
    {
        return max_damage_array[slot.getIndex()] * this.durability;
    }

    @Override
    public int getDefenseForSlot(EquipmentSlot slot)
    {
        return this.dmgReductionAmounts[slot.getIndex()];
    }

    @Override
    public int getEnchantmentValue()
    {
        return this.enchantability;
    }

    @Override
    public SoundEvent getEquipSound() {
        return this.soundEvent;
    }

    @Override
    public Ingredient getRepairIngredient()
    {
        return Ingredient.of(this.repairItem);
    }

    @Override
    public String getName()
    {
        return TemplateMain.MOD_ID + ":" + this.name;
    }

    @Override
    public float getToughness()
    {
        return this.toughness;
    }

    @Override
    public float getKnockbackResistance()
    {
        return this.knockbackResistance;
    }
}


package com.ladestitute.templatemod.util.datagen;

import com.ladestitute.templatemod.TemplateMain;
import com.ladestitute.templatemod.util.glm.TemplateDropModifier;
import net.minecraft.data.DataGenerator;
import net.minecraft.world.level.storage.loot.predicates.ExplosionCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.data.GlobalLootModifierProvider;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(modid = TemplateMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TemplateDataGen
{
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event)
    {
        DataGenerator generator = event.getGenerator();

        if (event.includeServer())
        {
            TemplateMain.LOGGER.debug("Starting Server Data Generators");
            TemplateTagsList.BlockTagsDataGen blockTagsProvider = new TemplateTagsList.BlockTagsDataGen(event.getGenerator(), event.getExistingFileHelper());
            // generator.addProvider(new RecipeOverrides(generator));
            // generator.addProvider(new LootTableDataGen(generator));
            generator.addProvider(new TemplateTagsList.ItemTagsDataGen(generator, blockTagsProvider, event.getExistingFileHelper()));
            generator.addProvider(new GLMProvider(generator));
        }
        if (event.includeClient())
        {
            TemplateMain.LOGGER.debug("Starting Client Data Generators");
        }
    }

    private static class GLMProvider extends GlobalLootModifierProvider {

        public GLMProvider(DataGenerator gen) {
            super(gen, TemplateMain.MOD_ID);
        }

        @Override
        protected void start() {
            add("template_drops", TemplateDropModifier.TEMPLATEMOD_DROPS.get(), new TemplateDropModifier.BlockDropModifier(
                    new LootItemCondition[] {
                            ExplosionCondition.survivesExplosion().build()
                    }));

        }
    }
}

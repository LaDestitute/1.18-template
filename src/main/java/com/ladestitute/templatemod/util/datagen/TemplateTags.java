package com.ladestitute.templatemod.util.datagen;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.Tags;

public class TemplateTags
{
    public static void init ()
    {
        Blocks.init();
        Items.init();
    }

    public static class Items
    {
        private static void init(){}

        public static final Tags.IOptionalNamedTag<Item> TEMPLATE_TAG_LIST = tag("template_tag_list");

        private static Tags.IOptionalNamedTag<Item> tag(String name)
        {
            return ItemTags.createOptional(new ResourceLocation("forge", name));
        }
    }
    public static class Blocks
    {
        private static void init(){}

        public static final Tags.IOptionalNamedTag<Block> ORES = tag("ores");

        private static Tags.IOptionalNamedTag<Block> tag(String name)
        {
            return BlockTags.createOptional(new ResourceLocation("forge", name));
        }
    }
}


package com.ladestitute.templatemod.util.datagen;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;

public class TemplateTagsList {
    public static class ItemTagsDataGen extends ItemTagsProvider {

        public ItemTagsDataGen(DataGenerator generatorIn, BlockTagsProvider blockTagsProvider,
                               ExistingFileHelper existingFileHelper) {
            super(generatorIn, blockTagsProvider, TemplateMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
            tag(TemplateTags.Items.TEMPLATE_TAG_LIST).add(Items.AMETHYST_BLOCK);
            tag(TemplateTags.Items.TEMPLATE_TAG_LIST).add(Items.AMETHYST_SHARD);

        }
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, TemplateMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
        }
    }
}

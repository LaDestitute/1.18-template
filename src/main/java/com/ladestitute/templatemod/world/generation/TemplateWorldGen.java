package com.ladestitute.templatemod.world.generation;

import com.ladestitute.templatemod.TemplateMain;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber(modid = TemplateMain.MOD_ID)
public class TemplateWorldGen {

        @SubscribeEvent
        public static void onBiomeLoad(BiomeLoadingEvent event) {
            TemplateResourceGen.generateResources(event);

            if (event.getCategory() == Biome.BiomeCategory.PLAINS){
                List<MobSpawnSettings.SpawnerData> spawns =
                        event.getSpawns().getSpawner(MobCategory.CREATURE);
                spawns.add(new MobSpawnSettings.SpawnerData(EntityType.SKELETON_HORSE, 4, 2, 3));

            }

        }
    }

package com.ladestitute.templatemod.world.generation;

import com.ladestitute.templatemod.registry.PlacedFeatureInit;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

public class TemplateResourceGen {

    public static void generateResources(final BiomeLoadingEvent event) {
        if (event.getName() == null)
            return;

        //There are other ways to generate stuff, this example is for getting specific biomes
        //Whether modded or vanilla
        //You could also check biome-category, biome type tags, temperature, etc
        if (event.getName().equals(new ResourceLocation("minecraft", "plains"))) {

            //    System.out.println("Added snow angels to " + event.getName());
            List<Supplier<PlacedFeature>> base =
                    event.getGeneration().getFeatures(GenerationStep.Decoration.LOCAL_MODIFICATIONS);

            base.add(() -> PlacedFeatureInit.TEMPLATE_PLACEMENT);
        }

    }
}

package com.ladestitute.templatemod;

import com.ladestitute.templatemod.registry.BlockInit;
import com.ladestitute.templatemod.registry.ItemInit;
import com.ladestitute.templatemod.registry.SoundInit;
import com.ladestitute.templatemod.registry.SpecialBlockInit;
import com.ladestitute.templatemod.util.TemplateConfig;
import com.ladestitute.templatemod.util.events.TemplateFoodEffectsHandler;
import com.ladestitute.templatemod.util.glm.TemplateDropModifier;
import net.minecraft.core.Direction;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(TemplateMain.MOD_ID)
@EventBusSubscriber(modid = TemplateMain.MOD_ID, bus = Bus.MOD)
public class TemplateMain
{
    //To make use of this template, just change the "modid"s in the build.gradle and mod.toml
    //Don't forget to rename the asset folders to your modid as well
    //Don't forget to refactor-rename packages or classes if desired
    //Also check resources, it has some example jsons you can copy/refer to
    //As well as the newer data-driven way of handling block tool requirements from +1.17 onward:
    // https://gist.github.com/gigaherz/691f528a61f631af90c9426c076a298a
    //This set of templates also has an access transformer already in
    //With some at-lines for structures in case you decide to implement some, with the AT-part already done.
    public static TemplateMain instance;
    public static final String MOD_NAME = "Template Mod";
    public static final String MOD_ID = "templatemod";
    public static final Logger LOGGER = LogManager.getLogger();

    public TemplateMain()
    {
        instance = this;
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, TemplateConfig.SPEC, "templatemodconfig.toml");
        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
        //Structures.DEFERRED_REGISTRY_STRUCTURE.register(modEventBus);
        modEventBus.addListener(this::setup);

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
        {
            //For stuff that should only be run client-side, such as a music ticker
           //MinecraftForge.EVENT_BUS.register(MusicHandler.class);
        });
        //Example mod event bus
        MinecraftForge.EVENT_BUS.register(new TemplateFoodEffectsHandler());
        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        //Some example registries to setup a basic mod
        TemplateDropModifier.GLM.register(modEventBus);
        SoundInit.SOUNDS.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
        SpecialBlockInit.SPECIAL_BLOCKS.register(modEventBus);
        BlockInit.BLOCKS.register(modEventBus);
    }

    //Helper method for setting the shape of custom-model blocks
    public static VoxelShape calculateShapes(Direction to, VoxelShape shape) {
        final VoxelShape[] buffer = { shape, Shapes.empty() };

        final int times = (to.get2DDataValue() - Direction.NORTH.get2DDataValue() + 4) % 4;
        for (int i = 0; i < times; i++) {
            buffer[0].forAllBoxes((minX, minY, minZ, maxX, maxY, maxZ) -> buffer[1] = Shapes.or(buffer[1],
                    Shapes.create(1 - maxZ, minY, minX, 1 - minZ, maxY, maxX)));
            buffer[0] = buffer[1];
            buffer[1] = Shapes.empty();
        }

        return buffer[0];
    }

    //Helper method to automatically register blocks and their item-forms
    @SubscribeEvent
    public static void createBlockItems(final RegistryEvent.Register<Item> event) {
        final IForgeRegistry<Item> registry = event.getRegistry();

        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> {
            final Item.Properties properties = new Item.Properties().tab(TemplateMain.BLOCKS_TAB);
            final BlockItem blockItem = new BlockItem(block, properties);
            blockItem.setRegistryName(block.getRegistryName());
            registry.register(blockItem);
        });
    }

    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {
        event.enqueueWork(() -> {
            //Uncomment if you are adding structures (TelepathicGrunt has an example-github on how)
           // BTDStructures.setupStructures();
           // BTDConfiguredStructures.registerConfiguredStructures();
        });
    }

    // Custom CreativeModeTab tab
    public static final CreativeModeTab BLOCKS_TAB = new CreativeModeTab("templatemod_blocks") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockInit.TEMPLATE_BLOCK.get());
        }
    };

}

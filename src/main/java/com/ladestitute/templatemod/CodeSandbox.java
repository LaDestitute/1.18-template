package com.ladestitute.templatemod;

import java.util.Random;

public class CodeSandbox {
    //A class for testing code

    //A snippet to store random for copy/pasting
    Random rand = new Random();
    //next int counts from zero and ignores the last number so you will only get possible results of 0-4 so keep this in mind
    int testint = rand.nextInt(5);
}
